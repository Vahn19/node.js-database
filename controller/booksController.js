const { books } = require('../models')

module.exports = {
    index: (req, res) => {
        books.findAll({}).then((books) => {
            if(books.length !== 0){
                res.json({
                    status: 200,
                    message: "Success",
                    data: books
                })
            }
            else{
                res.json({
                    status: 400,
                    message: "Data Empty"
                })
            }
        })
    },
    create: (req, res) => {
        const { isbn, judul, sinopsis, penulis, genre} = req.body;

        books.create({
            isbn,
            judul,
            sinopsis,
            penulis,
            genre
        }).then((books) => {
            res.json({
                status: 200,
                message: "Data Entry Success",
                data: books
            })
        })
    },
    update: (req, res) => {
        const { isbn, judul, sinopsis, penulis, genre} = req.body;
        const userId = req.params.id;
        const query = {
            where: {id: userId}
        }

        books.update(
            { isbn:'ID0186124',judul:'Judul Baru',sinopsis:'Data ini sudah diupdate',penulis:'Penulis Baru',genre:'data update'}, query
        )
        .then(() => {
            res.status(200),json("Update Success")
        })
        .catch((err) => {
            res.status(400).json("Update Failed")
        })
    },
    show: (req, res) => {
        const userId = req.params.id;

        books.findOne({
            where: {id: userId}
        })
        .then((data) => {
            res.json({
                status: 200,
                message: "Data Found",
                data: data
            })
        })
        .catch((err) => {
            res.json({
                status: 400,
                message: "Data Not Found"
            })
        })
    },
    delete: (req, res) => {
        const userId = req.params.id;

        books.destroy({
            where: {id: userId}
        })
        .then((data) => {
            res.json({
                status: 200,
                message: "Data Deleted",
                data: userId
            })
        })
        .catch((err) => {
            res.json({
                status: 400,
                message: "Data Failed to be Deleted"
            })
        })
    }
}