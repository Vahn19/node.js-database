const { books } = require('./models')

books.create({
    isbn: 'ID2110827',
    judul: 'Bootcamp SYNRGY',
    sinopsis: 'Acara bootcamp synrgy academy',
    penulis: 'Saya Sendiri',
    genre: 'Pelajaran'
}).then(book => {
    console.log(book)
}).catch(err => {
    console.log(err)
})