const router = require('express').Router()
const bookRouter = require('./booksRouter')

router.use('/books', bookRouter)

module.exports = router